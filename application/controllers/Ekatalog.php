<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ekatalog extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_ekatalog','eka');
	}

	public function index()
	{
	$data['tampil_eka']=$this->eka->tampil_eka();
	$data['konten']="v_ekatalog";
	$this->load->view('template', $data);
	}

	// public function get_tahun($tahun=''){
	// $bulan = $this->input->post('year'); // YYYY-MM
	//
	// $report_data = $this->M_ekatalog->get_data($tahun);
	// }

}
