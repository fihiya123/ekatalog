<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Masterdata extends CI_Controller {
	public function __construct()
	{
		parent::__construct();

		$this->load->model('M_data','seg');
		$this->load->model('M_data','treg');
		$this->load->model('M_data','witel');

	}

	public function index()
	{
	$data['tampil_segmen']=$this->seg->tampil_seg();
	$data['tampil_treg']=$this->treg->tampil_treg();
	$data['tampil_witel']=$this->witel->tampil_witel();
	$data['konten']="v_masterdata";
	$this->load->view('template', $data);

	}

}
