<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	function rupiah($angka)
	{
		$jadi = number_format($angka,0,',','.');
		return $jadi;
	}

	function tanggal_indo($tanggal)
	{
		$bulan = array (1 =>   'Januari',
					'Februari',
					'Maret',
					'April',
					'Mei',
					'Juni',
					'Juli',
					'Agustus',
					'September',
					'Oktober',
					'November',
					'Desember'
				);
		$split = explode('-', $tanggal);
		return $split[2] . ' ' . $bulan[ (int)$split[1] ] . ' ' . $split[0];
	}
	// function checkingsession(){
		// $CI =& get_instance();
		// $session_sess		= $CI->session->userdata('sess');


		// if(($session_sess == FALSE))
		// {
			// $CI->load->view("login/login");
		// }
		// else
		// {

			// $tmp['logsess'] = $session_sess;
			// $tmp['userid'] = $session_sess['USERID'];
			// $tmp['nama'] = $session_sess['NAMA'];
			// $tmp['username'] = $session_sess['USERNAME'];
			// $tmp['profile'] = $session_sess['ID_PROFILE'];

			// return $tmp ;
		// }
	// }



/* End of file Template.php */
/* Location: ./application/libraries/Template.php */
