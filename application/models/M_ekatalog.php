<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_ekatalog extends CI_Model {

	public function tampil_eka()
	{
		return $this->db
		->join('paket','e_katalog.id_paket=paket.id_paket')
		->join('status_paket','paket.id_status_paket=status_paket.id_status_paket')
		->join('posisi','paket.id_posisi=posisi.id_posisi')
		->join('status_negosiasi','e_katalog.id_status_negosiasi=status_negosiasi.id_status_negosiasi')
		->join('status','e_katalog.id_status=status.id_status')
		->join('periode','e_katalog.id_periode=periode.id_periode')
		->join('lop','e_katalog.id_lop=lop.id_lop')
		->get('e_katalog')->result();

	}
}
	// function get_data($tahun) {
	// 	$periode=array(
	// 		'periode'=>$this->input->post('periode'),
	// 	);
  //   $this->db->select('*');
  //   $this->db->from('e_katalog');
  //   $this->db->where('YEAR(tanggal_buat)',$tahun);
  //   $query = $this->db->get();
  //   if ($query->num_rows() > 0) {
  //       return $query->result();
  //       } else {
  //       return false;
  //   }


/* End of file tarif_model.php */
/* Location: ./application/models/tarif_model.php */
