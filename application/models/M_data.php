<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_data extends CI_Model {

    public function tampil_seg()
  {
    $tm_segmen = $this->db->get('segmen')->result();
    return $tm_segmen;
  }

  public function tampil_treg()
  {
  $tm_treg = $this->db->get('treg')->result();
  return $tm_treg;
  }

  public function tampil_witel()
  {
  $tm_witel = $this->db->get('witel')->result();
  return $tm_witel;
  }

}

/* End of file Tagihan_model.php */
/* Location: ./application/models/Tagihan_model.php */
