<!-- <div class="container-fluid"> -->
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="breadcome-list map-mg-t-40-gl shadow-reset">

                                    <div class="sparkline13-hd" style="background-color:#000080; font-family: century gothic">
                                    <div class="main-sparkline13-hd" >
                                        <h1 style="">Projects <span class="table-project-n">Data</span> Table</h1>

                                  <!--   </div> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
              <!--   </div> -->

               <!--  <div class="container-fluid"> -->
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="sparkline13-list shadow-reset">

                                <div class="sparkline13-graph">
                                    <div class="datatable-dashv1-list custom-datatable-overright">
                                        <!-- <div id="toolbar">
                                            <select class="form-control">
                                                <option value="">Export Basic</option>
                                                <option value="all">Export All</option>
                                                <option value="selected">Export Selected</option>
                                            </select>
                                        </div> -->
                                        <table id="table" data-toggle="table" data-pagination="true" data-search="true" data-show-columns="true" data-show-pagination-switch="true" data-show-refresh="true" data-key-events="true" data-show-toggle="true" data-resizable="true" data-cookie="true" data-cookie-id-table="saveId" data-show-export="true" data-click-to-select="true" data-toolbar="#toolbar">
                                          <thead style="background-color: #ebebe0;">
                                                <tr>
                                                    <th data-field="id"> No </th>
                                                    <th data-field="nomorpaket">Nomor Paket</th>
                                                    <th data-field="name">Nama Paket</th>
                                                    <th data-field="jumlahproduk">Jumlah Produk</th>
                                                    <th data-field="instansi">Instansi</th>
                                                    <th data-field="satker">Satker</th>
                                                    <th data-field="statuspaket">Status Paket</th>
                                                    <th data-field="posisi">Posisi Paket</th>
                                                    <th data-field="statusneg">Status Negosiasi</th>
                                                    <th data-field="tb">Tanggal Buat</th>
                                                    <th data-field="te">Tanggal Edit</th>
                                                    <th data-field="price">Total Harga</th>
                                                    <th data-field="status" style="width:20px;">Status</th>
                                                    <th data-field="produk"  style="width:60px;">Produk</th>
                                                    <th data-field="periode">Periode</th>
                                                    <th data-field="Treg">Treg</th>
                                                    <th data-field="witel">Witel</th>
                                                    <th data-field="idlop">ID LOP</th>
                                                    <th data-field="segmen">Segemen</th>
                                                    <th data-field="action">Action</th>

                                                </tr>
                                            </thead>
                                          <tbody>
                                            <?php $no=0;foreach ($tampil_eka as $eka):
                                                $no++;?>
                                                <tr>
                                                    <td><?=$no?></td>
                                                    <td><?=$eka->no_paket?></td>
                                                    <td><?=$eka->nama_paket?></td>
                                                    <td><?=$eka->jumlah_produk?></td>
                                                    <td></td>
                                                    <td> </td>
                                                    <td><?=$eka->nama_status_paket?></td>
                                                    <td><?=$eka->nama_posisi?></td>
                                                    <td><?=$eka->nama_negosiasi?></td>
                                                    <td><?=$eka->tanggal_buat?></td>
                                                    <td><?=$eka->tanggal_edit?></td>
                                                    <td><?=($eka->jumlah_produk*$eka->harga)?></td>
                                                    <td><?=$eka->nama_status?></td>
                                                    <td><?=$eka->nama_produk?></td>
                                                    <td><?=$eka->periode?></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td><?=$eka->id_lop?></td>
                                                    <td></td>
                                                     <td>
                                                    <a class="zoomInDown mg-t" href="#" data-toggle="modal"
                                                    data-target="#zoomInDown1"><span class="btn btn-info fa fa-edit"></span></a>
                                                     <!-- <a class="zoomInDown mg-t" href="#" data-toggle="modal"
                                                     data-target="#DangerModalftblack">
                                                     <span class="btn btn-danger fa fa-trash"></span></a> -->
                                                    </td>
                                                  <?php endforeach ?>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
        <!-- Modals -->
<div id="DangerModalftblack" class="modal modal-adminpro-general FullColor-popup-DangerModal PrimaryModal-bgcolor fade" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-close-area modal-close-df">
                                            <a class="close" data-dismiss="modal" href="#"><i class="fa fa-close"></i></a>
                                        </div>
                                        <div class="modal-body">
                                            <span class="adminpro-icon adminpro-danger-error modal-check-pro information-icon-pro"></span>
                                            <h2>Delete!</h2>
                                            <p>Apakah anda yakin ingin menghapus?</p>
                                        </div>
                                        <div class="modal-footer footer-modal-admin">
                                            <a data-dismiss="modal" href="#">Cancel</a>
                                            <a href="#">Delete</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
  <div id="PrimaryModalftblack" class="modal modal-adminpro-general default-popup-PrimaryModal PrimaryModal-bgcolor fade" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-close-area modal-close-df">
                                            <a class="close" data-dismiss="modal" href="#"><i class="fa fa-close"></i></a>
                                        </div>
                                        <div class="modal-body">
                                            <i class="fa fa-check modal-check-pro"></i>
                                            <h2>Awesome!</h2>
                                            <p>The Modal plugin is a dialog box/popup window that is displayed on top of the current page</p>
                                        </div>
                                        <div class="modal-footer footer-modal-admin">
                                            <a data-dismiss="modal" href="#">Cancel</a>
                                            <a href="#">Process</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
<!-- update -->
<div id="zoomInDown1" class="modal modal-adminpro-general modal-zoomInDown fade zoomInDown animated" role="dialog" style="display: none;">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-close-area modal-close-df">
                                    <a class="close" data-dismiss="modal" href="#"><i class="fa fa-close"></i></a>
                                </div>
                                <div class="modal-body">
                                    <div class="modal-login-form-inner">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="basic-login-inner modal-basic-inner">
                                                    <h3>Sign In</h3>
                                                    <p>
                                                        Register User can get sign in from here
                                                    </p>
                                                    <form action="#">
                                                        <div class="form-group-inner">
                                                            <div class="row">
                                                                <div class="col-lg-4">
                                                                    <label class="login2">Email</label>
                                                                </div>
                                                                <div class="col-lg-8">
                                                                    <input type="email" class="form-control" placeholder="Enter Email"></div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group-inner">
                                                            <div class="row">
                                                                <div class="col-lg-4">
                                                                    <label class="login2">Password</label>
                                                                </div>
                                                                <div class="col-lg-8">
                                                                    <input type="password" class="form-control" placeholder="password"></div>
                                                            </div>
                                                        </div>
                                                        <div class="login-btn-inner">
                                                            <div class="row">
                                                                <div class="col-lg-4"></div>
                                                                <div class="col-lg-8">
                                                                    <div class="login-horizental">
                                                                        <button class="btn btn-sm btn-primary login-submit-cs" type="submit">Sign In</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
<script type="text/javascript">
    $(".Mytable1").dataTable();
</script>
