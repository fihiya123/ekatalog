<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>E- Katalog</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- favicon
        ============================================ -->
    <link rel="shortcut icon" type="image/x-icon"  href="<?php echo base_url();?>assets/img/favicon.ico">
    <!-- Google Fonts
        ============================================ -->
    <link  href="<?php echo base_url();?>assets/https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i,800" rel="stylesheet">
    <!-- Bootstrap CSS
        ============================================ -->
    <link rel="stylesheet"  href="<?php echo base_url();?>assets/css/bootstrap.min.css">
    <!-- Bootstrap CSS
        ============================================ -->
    <link rel="stylesheet"  href="<?php echo base_url();?>assets/css/font-awesome.min.css">
    <!-- adminpro icon CSS
        ============================================ -->
    <link rel="stylesheet"  href="<?php echo base_url();?>assets/css/adminpro-custon-icon.css">
    <!-- meanmenu icon CSS
        ============================================ -->
    <link rel="stylesheet"  href="<?php echo base_url();?>assets/css/meanmenu.min.css">
    <!-- mCustomScrollbar CSS
        ============================================ -->
    <link rel="stylesheet"  href="<?php echo base_url();?>assets/css/jquery.mCustomScrollbar.min.css">
    <!-- animate CSS
        ============================================ -->
    <link rel="stylesheet"  href="<?php echo base_url();?>assets/css/animate.css">
    <!-- data-table CSS
        ============================================ -->
    <link rel="stylesheet"  href="<?php echo base_url();?>assets/css/data-table/bootstrap-table.css">
    <link rel="stylesheet"  href="<?php echo base_url();?>assets/css/data-table/bootstrap-editable.css">
    <!-- normalize CSS
        ============================================ -->
    <link rel="stylesheet"  href="<?php echo base_url();?>assets/css/normalize.css">
    <!-- charts C3 CSS
        ============================================ -->
    <link rel="stylesheet"  href="<?php echo base_url();?>assets/css/c3.min.css">
    <!-- forms CSS
        ============================================ -->
    <link rel="stylesheet"  href="<?php echo base_url();?>assets/css/form/all-type-forms.css">
    <!-- style CSS
        ============================================ -->
    <link rel="stylesheet"  href="<?php echo base_url();?>assets/style.css">
    <link rel="stylesheet"  href="<?php echo base_url();?>assets/css/modals.css">
    <!-- responsive CSS
        ============================================ -->
    <link rel="stylesheet"  href="<?php echo base_url();?>assets/css/responsive.css">
    <!-- modernizr JS
        ============================================ -->
    <script  src="<?php echo base_url();?>assets/js/vendor/modernizr-2.8.3.min.js"></script>

     <link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
    <link href="<?php echo base_url()?>assets/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">




    <!-- Jquery Core Js -->
    <script src="<?php echo base_url()?>assets/plugins/jquery/jquery.min.js"></script>
            <!-- Jquery DataTable Plugin Js -->
    <script src="<?php echo base_url()?>assets/plugins/jquery-datatable/jquery.dataTables.js"></script>
    <script src="<?php echo base_url()?>assets/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
    <script src="<?php echo base_url()?>assets/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url()?>assets/plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
    <script src="<?php echo base_url()?>assets/plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
    <script src="<?php echo base_url()?>assets/plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
    <script src="<?php echo base_url()?>assets/plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
    <script src="<?php echo base_url()?>assets/plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
    <script src="<?php echo base_url()?>assets/plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>
</head>

<body class="materialdesign">
    <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a  href="<?php echo base_url();?>assets/http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

    <!-- Header top area start-->
    <div class="wrapper-pro">
        <div class="left-sidebar-pro">
            <nav id="sidebar">
                <div class="sidebar-header">
                    <a  href="<?php echo base_url();?>assets/#"><img  src="<?php echo base_url();?>assets/img/message/1.jpg" alt="" />
                    </a>
                    <h3>Andrar Son</h3>
                    <p>Developer</p>
                    <strong></strong>
                </div>
                <div class="left-custom-menu-adp-wrap">
                    <ul class="nav navbar-nav left-sidebar-menu-pro">
                        <li><a href="<?php echo base_url();?>index.php/Dashboard"style=""><i class="fa big-icon fa-home"></i> <span class="mini-dn">Home</span> <span class=""><i class=""></i></span></a></li>

                         <li><a href="<?php echo base_url();?>index.php/Ekatalog"style=""><i class="fa big-icon fa-table"></i> <span class="mini-dn">Ekatalog</span> <span class=""><i class=""></i></span></a></li>

                         <li><a href="<?php echo base_url();?>index.php/Ekatalog2"style=""><i class="fa big-icon fa-table"></i> <span class="mini-dn">Ekatalog2</span> <span class=""><i class=""></i></span></a></li>

                         <li><a href="<?php echo base_url();?>index.php/Masterdata"style=""><i class="fa big-icon fa-table"></i> <span class="mini-dn">Master Data</span> <span class=""><i class=""></i></span></a></li>

                    <!--   <li class="nav-item">
                            <a  href="<?php echo base_url();?>assets/#" data-toggle="dropdown" role="button" aria-expanded="false" class="nav-link dropdown-toggle"><i class="fa big-icon fa-table"></i> <span class="mini-dn">Master Data</span> <span class="indicator-right-menu mini-dn"><i class="fa indicator-mn fa-angle-left"></i></span></a>
                            <div role="menu" class="dropdown-menu left-menu-dropdown animated flipInX">
                                <a  href="<?php echo base_url();?>index.php/Witel" class="dropdown-item">Witel</a>
                                <a  href="<?php echo base_url();?>assets/" class="dropdown-item">T-Reg</a>
                                <a  href="<?php echo base_url();?>assets/" class="dropdown-item">Segment</a>
                            </div>
                        </li> -->
                    </ul>
                </div>
            </nav>
        </div>
        <div class="content-inner-all">
            <div class="header-top-area">
                <div class="fixed-header-top">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-1 col-md-6 col-sm-6 col-xs-12">
                                <button type="button" id="sidebarCollapse" class="btn bar-button-pro header-drl-controller-btn btn-info navbar-btn">
                                    <i class="fa fa-bars"></i>
                                </button>
                                <div class="admin-logo logo-wrap-pro">
                                    <a  href="<?php echo base_url();?>assets/#"><img  src="<?php echo base_url();?>assets/img/logo/log.png" alt="" />
                                    </a>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-1 col-sm-1 col-xs-12">

                            </div>
                            <div class="col-lg-5 col-md-5 col-sm-6 col-xs-12">
                                <div class="header-right-info">
                                    <ul class="nav navbar-nav mai-top-nav header-right-menu">

                                        <li class="nav-item">
                                            <a  href="<?php echo base_url();?>assets/#" data-toggle="dropdown" role="button" aria-expanded="false" class="nav-link dropdown-toggle">
                                                <span class="adminpro-icon adminpro-user-rounded header-riht-inf"></span>
                                                <span class="admin-name">Advanda Cro</span>
                                                <span class="author-project-icon adminpro-icon adminpro-down-arrow"></span>
                                            </a>
                                            <ul role="menu" class="dropdown-header-top author-log dropdown-menu animated flipInX">
                                                <li><a  href="<?php echo base_url();?>assets/#"><span class="adminpro-icon adminpro-user-rounded author-log-ic"></span>My Profile</a>
                                                </li>
                                                <li><a href="<?PHP echo base_url('index.php/logout')?>">
                                                  <span class="adminpro-icon adminpro-locked author-log-ic"></span>Log Out</a>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Header top area end-->
            <!-- Breadcome start-->


            <!-- Breadcome End-->
            <!-- welcome Project, sale area start-->
            <div class="welcome-adminpro-area">
                <div class="container-fluid" >
                    <?php
                         $this->load->view($konten);
                    ?>
                </div>
            </div>
        </div>
    </div>

    <!-- Footer Start-->
   <!--  <div class="footer-copyright-area">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="footer-copy-right">
                        <p>Copyright &#169; 2018 Colorlib All rights reserved. Template by <a  href="<?php echo base_url();?>assets/https://colorlib.com">Colorlib</a>.</p>
                    </div>
                </div>
            </div>
        </div>
    </div> -->
    <!-- Footer End-->
    <!-- Chat Box Start-->

    <!-- Chat Box End-->

     <script src="<?php echo base_url();?>assets//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <!-- jquery
        ============================================ -->
    <script  src="<?php echo base_url();?>assets/js/vendor/jquery-1.11.3.min.js"></script>
    <!-- bootstrap JS
        ============================================ -->
    <script  src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
    <!-- meanmenu JS
        ============================================ -->
    <script  src="<?php echo base_url();?>assets/js/jquery.meanmenu.js"></script>
    <!-- mCustomScrollbar JS
        ============================================ -->
    <script  src="<?php echo base_url();?>assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
    <!-- sticky JS
        ============================================ -->
    <script  src="<?php echo base_url();?>assets/js/jquery.sticky.js"></script>
    <!-- scrollUp JS
        ============================================ -->
    <script  src="<?php echo base_url();?>assets/js/jquery.scrollUp.min.js"></script>
    <!-- counterup JS
        ============================================ -->
    <script  src="<?php echo base_url();?>assets/js/counterup/jquery.counterup.min.js"></script>
    <script  src="<?php echo base_url();?>assets/js/counterup/waypoints.min.js"></script>
    <script  src="<?php echo base_url();?>assets/js/counterup/counterup-active.js"></script>
    <!-- peity JS
        ============================================ -->
    <script  src="<?php echo base_url();?>assets/js/peity/jquery.peity.min.js"></script>
    <script  src="<?php echo base_url();?>assets/js/peity/peity-active.js"></script>
    <!-- sparkline JS
        ============================================ -->
    <script  src="<?php echo base_url();?>assets/js/sparkline/jquery.sparkline.min.js"></script>
    <script  src="<?php echo base_url();?>assets/js/sparkline/sparkline-active.js"></script>
    <!-- flot JS
        ============================================ -->
    <script  src="<?php echo base_url();?>assets/js/flot/jquery.flot.js"></script>
    <script  src="<?php echo base_url();?>assets/js/flot/jquery.flot.tooltip.min.js"></script>
    <script  src="<?php echo base_url();?>assets/js/flot/jquery.flot.spline.js"></script>
    <script  src="<?php echo base_url();?>assets/js/flot/jquery.flot.resize.js"></script>
    <script  src="<?php echo base_url();?>assets/js/flot/jquery.flot.pie.js"></script>
    <script  src="<?php echo base_url();?>assets/js/flot/Chart.min.js"></script>
    <script  src="<?php echo base_url();?>assets/js/flot/flot-active.js"></script>
    <!-- map JS
        ============================================ -->
    <script  src="<?php echo base_url();?>assets/js/map/raphael.min.js"></script>
    <script  src="<?php echo base_url();?>assets/js/map/jquery.mapael.js"></script>
    <script  src="<?php echo base_url();?>assets/js/map/france_departments.js"></script>
    <script  src="<?php echo base_url();?>assets/js/map/world_countries.js"></script>
    <script  src="<?php echo base_url();?>assets/js/map/usa_states.js"></script>
    <script  src="<?php echo base_url();?>assets/js/map/map-active.js"></script>
    <!-- data table JS
        ============================================ -->
    <script  src="<?php echo base_url();?>assets/js/data-table/bootstrap-table.js"></script>
    <script  src="<?php echo base_url();?>assets/js/data-table/tableExport.js"></script>
    <script  src="<?php echo base_url();?>assets/js/data-table/data-table-active.js"></script>
    <script  src="<?php echo base_url();?>assets/js/data-table/bootstrap-table-editable.js"></script>
    <script  src="<?php echo base_url();?>assets/js/data-table/bootstrap-editable.js"></script>
    <script  src="<?php echo base_url();?>assets/js/data-table/bootstrap-table-resizable.js"></script>
    <script  src="<?php echo base_url();?>assets/js/data-table/colResizable-1.5.source.js"></script>
    <script  src="<?php echo base_url();?>assets/js/data-table/bootstrap-table-export.js"></script>
    <!-- main JS
        ============================================ -->
    <script  src="<?php echo base_url();?>assets/js/main.js"></script>

    <script type="text/javascript">

      $('#myTable1').dataTable( {
          "pageLength": 3
        } );

    </script>
<!-- oo -->
</body>

</html>
