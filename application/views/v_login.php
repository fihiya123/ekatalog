<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>E - Katalog</title>
    <!-- plugins:css -->
    <link rel="stylesheet" href="<?PHP echo base_url(); ?>asset/vendors/iconfonts/mdi/css/materialdesignicons.min.css">
    <link rel="stylesheet" href="<?PHP echo base_url(); ?>asset/vendors/iconfonts/ionicons/css/ionicons.css">
    <link rel="stylesheet" href="<?PHP echo base_url(); ?>asset/vendors/iconfonts/typicons/src/font/typicons.css">
    <link rel="stylesheet" href="<?PHP echo base_url(); ?>asset/vendors/iconfonts/flag-icon-css/css/flag-icon.min.css">
    <link rel="stylesheet" href="<?PHP echo base_url(); ?>asset/vendors/css/vendor.bundle.base.css">
    <link rel="stylesheet" href="<?PHP echo base_url(); ?>asset/vendors/css/vendor.bundle.addons.css">
    <!-- endinject -->
    <!-- plugin css for this page -->
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <link rel="stylesheet" href="<?PHP echo base_url(); ?>asset/css/shared/style.css">
    <!-- endinject -->
    <link rel="shortcut icon" href="<?PHP echo base_url(); ?>asset/images/logo1.png" />
  </head>
  <body style="background-image: url('<?PHP echo base_url(); ?>asset/images/background-image-example.jpg');">
    <div class="container-scroller">

      <div class="container-fluid page-body-wrapper full-page-wrapper">
        <div class="content-wrapper d-flex align-items-center auth auth-bg-1 theme-one">
          <div class="row w-100">

            <div class="col-lg-4 mx-auto" >

              <div class="auto-form-wrapper" style="background-color: transparent;">

                <img src="<?php echo base_url();?>asset/images/logoku.png" style="text-align: center; padding-right: 90%;">
                <br>
                <br>
                <br>
               <form action="<?PHP echo base_url()?>index.php/login/proses_login" id="sign_in" method="POST">

                 <div class="msg"></div>
                    <?php if ($this->session->flashdata('pesan')!=null): ?>
                        <div class="alert alert-warning"><?= $this->session->flashdata('pesan');?></div>
                    <?php endif ?>

                  <div class="form-group">
                    <label class="label" style="color: white;">Username</label>
                    <div class="input-group" >
                      <input type="text" class="form-control" name="username" placeholder="Username">
                      <div class="input-group-append">
                        <span class="input-group-text">
                          <i class="mdi mdi-check-circle-outline"></i>
                        </span>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="label" style="color: white;">Password</label>
                    <div class="input-group">
                      <input type="password" class="form-control" name="password" placeholder="*********">
                      <div class="input-group-append">
                        <span class="input-group-text">
                          <i class="mdi mdi-check-circle-outline"></i>
                        </span>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <button class="btn btn-primary submit-btn btn-block" type="submit">Login</button>
                  </div>
                </form>
              </div>
              <ul class="auth-footer">
                <li>
                  <a href="#"></a>
                </li>
                <li>
                  <a href="#"></a>
                </li>
                <li>
                  <a href="#"></a>
                </li>
              </ul>
              <p class="footer-text text-center">Made By Grs Assik with <img src="<?php echo base_url();?>asset/images/like.png"></p>
            </div>
          </div>
        </div>
        <!-- content-wrapper ends -->
      </div>
      <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->
    <!-- plugins:js -->
    <script src="<?PHP echo base_url(); ?>asset/vendors/js/vendor.bundle.base.js"></script>
    <script src="<?PHP echo base_url(); ?>asset/vendors/js/vendor.bundle.addons.js"></script>
    <!-- endinject -->
    <!-- inject:js -->
    <script src="<?PHP echo base_url(); ?>asset/js/shared/off-canvas.js"></script>
    <script src="<?PHP echo base_url(); ?>asset/js/shared/misc.js"></script>
    <!-- endinject -->
  </body>
</html>
